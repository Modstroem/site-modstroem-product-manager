﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPM
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbllogInd.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString().Replace(@"AKMOD\", "").Replace(".", " ");
                if (lbllogInd.Text != "")
                {
                    try
                    {
                        string[] names = lbllogInd.Text.Split(' ');
                        string name = "";
                        foreach (string n in names) { name += FirstLetterToUpper(n)+ " "; }
                        lbllogInd.Text = name;
                    }
                    catch { }
                }
            }

        }
        public string FirstLetterToUpper(string str)
        {
            if (str == null)
                return "";

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }
    }
}